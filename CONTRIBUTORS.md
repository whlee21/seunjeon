# seunjeon contributors

* **[paul kim](https://bitbucket.org/paul_master/)**
  * 연접비용 계산 버그 수정
* **[Hangloug Lee](https://bitbucket.org/akaroice/)**
  * Windows OS 에서 돌아가도록 구현
* **[Wan-Hee Lee](https://bitbucket.org/whlee21/)**
  * 사용사 사전 로딩 버그 수정
  * scala 2.10, 2.11 cross-compile 지원
